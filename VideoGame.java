public class VideoGame {

	public String name;
	public String genre;
	public double price;

	public void gameDescription(String name, String genre, double price) {
		
		System.out.print(name + " is a " + genre + " that cost " + price + "$.");
		
	}

}