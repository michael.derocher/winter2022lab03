public class Shop {

	public static void main(String[] args) {
	
		java.util.Scanner reader = new java.util.Scanner(System.in);
	
		VideoGame[] games = new VideoGame[4];
	
		for (int count = 0; count < 4; count++){
			
			games[count] = new VideoGame();
			
			System.out.println("Game " + (count+1) + ":");
			
			System.out.println("What is the game's title?");
			games[count].name = reader.nextLine();
			System.out.println();
			
			System.out.println("What is the game's genre?");
			games[count].genre = reader.nextLine();
			System.out.println();
			
			System.out.println("What is the game's price?");
			games[count].price = reader.nextDouble();
			System.out.println();
			
			reader.nextLine();	
		}
		
		System.out.println(games[3].name + "  " + games[3].genre + "  " + games[3].price);
		
		games[3].gameDescription(games[3].name, games[3].genre, games[3].price);
		
	}
}